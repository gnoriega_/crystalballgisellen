package android.noriegag.crystalball;

public class Predictions {

    public static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
        "Your wish will come true!... I think",
                "Meow",
                "Your wishes will NEVA come true. jk",
                "Cat Powers Activate!",
                "Nyanyanyanyan!!"


    };

}
    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();

        }
        return predictions;
    }
    public String getPrediction() {
        int range = (answers.length -1);
        int rand = (int) (Math.random()*range) +1;
        return answers[rand];
    }
}
